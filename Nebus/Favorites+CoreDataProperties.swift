//
//  Favorites+CoreDataProperties.swift
//  
//
//  Created by Andres David Laiton on 22/10/19.
//
//

import Foundation
import CoreData


extension Favorites {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Favorites> {
        return NSFetchRequest<Favorites>(entityName: "Favorites")
    }

    @NSManaged public var busRouteName: String?

}
