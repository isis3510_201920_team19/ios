//
//  UserPoints+CoreDataProperties.swift
//  
//
//  Created by Andres David Laiton on 14/12/19.
//
//

import Foundation
import CoreData


extension UserPoints {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UserPoints> {
        return NSFetchRequest<UserPoints>(entityName: "UserPoints")
    }

    @NSManaged public var points: Int32

}
