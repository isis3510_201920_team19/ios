//
//  ViewController.swift
//  Nebus
//
//  Created by Andres David Laiton on 10/4/19.
//  Copyright © 2019 Andres David Laiton. All rights reserved.
//
// APP ID: ca-app-pub-1332202243111909~4118208179
// UNIT ID: ca-app-pub-1332202243111909/1021271545

import UIKit
import Firebase
import GoogleMobileAds
import Mapbox
import MapboxCoreNavigation
import MapboxNavigation
import MapboxDirections
import CoreData
import Foundation
import SystemConfiguration
import UserNotifications

class ViewController: UIViewController {
    
    //MARK: Variables
    //Favorites button
    @IBOutlet weak var favoritesButton: UIButton!
    //Search Bar
    @IBOutlet weak var searchBarBus: UISearchBar!
    //Table view of the bus routes
    @IBOutlet weak var busRoutesTableView: UITableView!
    //variable to change the Height of busRoutesTable
    @IBOutlet weak var busRoutesTableHeight: NSLayoutConstraint!
    //Table view of the times of the selected bus
    @IBOutlet weak var busTimesTableView: UITableView!
    //variable to change the Height of busTimesTable
    @IBOutlet weak var busTimesTableHeight: NSLayoutConstraint!
    //Table view of the favorites routes
    @IBOutlet weak var favoritesTableView: UITableView!
    //Variable to change the Height of the favoritesTable
    @IBOutlet weak var favoritesTableHeight: NSLayoutConstraint!
    //Table view of the selected bus
    @IBOutlet weak var selectedBusTableView: UITableView!
    //Variable to change the Height of the selectedBusTable
    @IBOutlet weak var selectedBusTableHeight: NSLayoutConstraint!
    //Current points label
    @IBOutlet weak var currentUserPoints: UILabel!
    var offlineMapProgress: UIProgressView!
    
    //Variable for the manager of the MapView
    private let locationManager = CLLocationManager()
    //Variable for the map view
    @IBOutlet weak var mapView: NavigationMapView!
    var directionsRoute: Route?
    
    //Variable for the bus currently searching
    var searchedBus = [String]()
    //Variable for know if the user is searching
    var searching = false
    
    var startedSearching = false
    
    var showingTimes = false
    
    var showingFavorites = false
    
    var showingSelectedBus = false
    
    //Bus routes
    let busRoutesArr = ["18-3 Lijacá", "18-3 Germania", "", ""]
    
    var busRoutesTimes = [Int]()
    
    var loadingIndicator = UIActivityIndicatorView()
    
    var favorites = [Favorites]()
    
    var userPoints = Int32()
    
    var selectedStops = [DocumentSnapshot]()
    
    var timeOfTravel = String()
    
    var timeArrivingBus = Double()
    
    var numberOfBusSelected = 5
    
    var notifyUser = false
    
    var cancelButtonPressed = false
    
    var interstitial: GADInterstitial!
    //MARK: Override Methods
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBarBus.setPlaceholder(textColor: .white)
        
        //Hide keyboard
        self.HideKeyboard()
        
        //Make the corners rounded
        favoritesButton.layer.cornerRadius = 10.0
        currentUserPoints.layer.cornerRadius = 10.0
        
        currentUserPoints.layer.borderColor = UIColor.white.cgColor
        
        //Search bus information
        searchBarBus.delegate = self
        
        favoritesFetch()
        currentUserPointsFetch()
        
        //Location things to work proprertly
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestAlwaysAuthorization()
        
        mapView.delegate = self
        //        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        //        locationManager.distanceFilter = 50Listo
        
        //Hide the favorites button
        favoritesButton.isHidden = true
        
        mapView.allowsTilting = false
        
        // Setup offline pack notification handlers.
        NotificationCenter.default.addObserver(self, selector: #selector(offlinePackProgressDidChange), name:NSNotification.Name.MGLOfflinePackProgressChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(offlinePackDidReceiveError), name: NSNotification.Name.MGLOfflinePackError, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(offlinePackDidReceiveMaximumAllowedMapboxTiles), name: NSNotification.Name.MGLOfflinePackMaximumMapboxTilesReached, object: nil)
        
        //Ads settings
        interstitial = GADInterstitial(adUnitID: "ca-app-pub-3940256099942544/4411468910")
        let request = GADRequest()
        interstitial.load(request)
    }
    
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func showAlert() -> Bool {
        var conditionToReturn = false
        if !isInternetAvailable() {
            let alert = UIAlertController(title: "Aviso", message: "No hay conexión a internet disponible", preferredStyle: .alert)
            let action = UIAlertAction(title: "Aceptar", style: .default, handler: nil)
            alert.addAction(action)
            present(alert, animated: true, completion: nil)
            conditionToReturn = true
        }
        return conditionToReturn
    }
    
    func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    
    @IBAction func favoritesButton(_ sender: Any) {
        let selectedRoute = searchBarBus.text
        
        let favoriteRoute = Favorites(context: PersistenService.context)
        favoriteRoute.busRouteName = selectedRoute
        PersistenService.saveContext()
        
        //Hide the favorites button
        favoritesButton.isHidden = true
        favoritesFetch()
    }
    
    func showNoPointsAd() {
        if interstitial.isReady {
          interstitial.present(fromRootViewController: self)
            interstitial = createAndLoadInterstitial()
        } else {
          print("Ad wasn't ready")
        }
    }
    
    func createAndLoadInterstitial() -> GADInterstitial {
      let interstitial = GADInterstitial(adUnitID: "ca-app-pub-3940256099942544/4411468910")
      interstitial.load(GADRequest())
      return interstitial
    }
    
    func favoritesFetch(){
        //Fetch the favorites data
        let fetchRequestFavorites: NSFetchRequest<Favorites> = Favorites.fetchRequest()
        
        do {
            let favorites = try PersistenService.context.fetch(fetchRequestFavorites)
            self.favorites = favorites
            let countFavorites = favorites.count
            if countFavorites > 0 {
                showingFavorites = true
                favoritesTableView.reloadData()
            }
        } catch {
            print("Fetching core data error")
        }
    }
    
    func currentUserPointsFetch(){
        //Fetch the user points data
        let fetchRequestUserPoints: NSFetchRequest<UserPoints> = UserPoints.fetchRequest()
        
        do {
            let userPoints = try PersistenService.context.fetch(fetchRequestUserPoints)
            if userPoints.count > 0 {
                self.userPoints = userPoints[0].points
                currentUserPoints.text = "Puntos actuales: \(self.userPoints)"
            } else {
                let userPoints = UserPoints(context: PersistenService.context)
                userPoints.points = 20
                PersistenService.saveContext()
                currentUserPoints.text = "Puntos actuales: 20"
            }
        } catch {
            print("Fetching core data error")
        }
    }
    
    func substractUserPoints() {
        let fetchRequestUserPoints: NSFetchRequest<UserPoints> = UserPoints.fetchRequest()
        do {
            let userPointsArray = try PersistenService.context.fetch(fetchRequestUserPoints)
            if userPointsArray[0].points > 0 {
                userPointsArray[0].points = userPointsArray[0].points - 10
                self.userPoints = userPointsArray[0].points
                PersistenService.saveContext()
                currentUserPoints.text = "Puntos actuales: \(self.userPoints)"
            } else {
                //Show alert
                let alert = UIAlertController(title: "¡Te quedaste sin puntos!", message: "Actualmente no cuentas con puntos. Por esta razón te vamos a mostrar un anuncio. Para evitar que veas anuncios acumula puntos al usar Nebus cuando estás en el bus.", preferredStyle: .alert)
                let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: { action in
                    self.showNoPointsAd()
                } )
                alert.addAction(aceptar)
                present(alert, animated: true, completion: nil)
            }
        } catch {
            print("Fetching core data error")
        }
    }
    
}


//MARK: UITableViewDelegate

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    //resize the table depending on the number of elements to show.
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var toReturn = Int()
        if tableView == busRoutesTableView || tableView == busTimesTableView{
            if searching {
                updateTablesSize(searchedBus.count)
                toReturn = searchedBus.count
            } else {
                updateTablesSize(busRoutesArr.count)
                toReturn = busRoutesArr.count
            }
        } else if tableView == favoritesTableView {
            toReturn = favorites.count
        } else if tableView == selectedBusTableView {
            toReturn = 1
        }
        return toReturn
    }
    
    //updates the tables
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "BusCell")
        if tableView == busRoutesTableView {
            //Verify if the user has started typing something and filter the results of that search
            if searching {
                cell?.textLabel?.text = searchedBus[indexPath.row]
            } else {
                cell?.textLabel?.text = busRoutesArr[indexPath.row]
            }
        } else if tableView == busTimesTableView{
            cell = tableView.dequeueReusableCell(withIdentifier: "BusTimeCells")
            
            //get the expected times as an array on Ints
            let minutesBusToArrive = self.busRoutesTimes[indexPath.row]
            
            //Get the current time of the device
            let date = Date()
            let newDate = date.addingTimeInterval(TimeInterval(minutesBusToArrive*60))
            
            //Formatter of the date to print in the table
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "HH:mm"
            
            //Add the expected minutes of bus arrival to show it propertly in the table
            let timeToPrintInTable = dateFormatterPrint.string(from: newDate)
            
            //Assign the text to the cell label
            cell?.textLabel?.text = "(\(timeToPrintInTable)) Llega en ~\(minutesBusToArrive) min"
            
             
            
        } else if tableView == favoritesTableView {
            cell = tableView.dequeueReusableCell(withIdentifier: "FavoritesCells")
            cell?.textLabel?.text = favorites[indexPath.row].busRouteName!
        } else if tableView == selectedBusTableView {
            //get the expected times as an array on Ints
            let minutesBusToArrive = self.timeArrivingBus
            
            //Get the current time of the device
            let date = Date()
            let newDate = date.addingTimeInterval(TimeInterval(minutesBusToArrive*60))
            
            //Formatter of the date to print in the table
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "HH:mm"
            
            //Add the expected minutes of bus arrival to show it propertly in the table
            let timeToPrintInTable = dateFormatterPrint.string(from: newDate)
            
            
            cell = tableView.dequeueReusableCell(withIdentifier: "SelectedBusCells")
            cell?.textLabel?.text = "El bus seleccionado llega a las \(timeToPrintInTable)"
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if !(tableView == favoritesTableView) {
            return false
        }
        else {
            return true
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if tableView == favoritesTableView {
            guard editingStyle == .delete else {return}
            let favoriteToDelete = favorites.remove(at: indexPath.row)
            PersistenService.context.delete(favoriteToDelete)
            PersistenService.saveContext()
            tableView.deleteRows(at: [indexPath], with: .automatic)
            favoritesFetch()
            updateTablesSize(0)
        }
    }
    
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        if tableView == busRoutesTableView {
            if searchedBus.count > 0 {
                searchBarBus.text = searchedBus[indexPath.row]
            } else {
                searchBarBus.text = busRoutesArr[indexPath.row]
            }
            if !showAlert() {
                selectRoute(for: self.searchBarBus.text!)
                startedSearching = false
                updateTablesSize(0)
            }
        } else if tableView == busTimesTableView {
            UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
            showingTimes = false
            numberOfBusSelected = indexPath.row
            timeArrivingBus = Double(busRoutesTimes[indexPath.row])
            let travelTime = Double(timeOfTravel)!
            if timeArrivingBus > travelTime {
                let notificationTime = timeArrivingBus - travelTime
                
                //Notification settings
                let content = UNMutableNotificationContent()
                content.title = "¡Tu bus ya va a llegar!"
                content.body = "Sal ya para que puedas tomar el bus que seleccionaste"
                content.sound = UNNotificationSound.default
                
                let trigger = UNTimeIntervalNotificationTrigger(timeInterval: notificationTime * 60, repeats: false)
                
                let request = UNNotificationRequest(identifier: "busArrived", content: content, trigger: trigger)
                
                UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
                
                substractUserPoints()
            } else {
                let alert = UIAlertController(title: "Aviso", message: "El tiempo estimado de llegada del bus es de \(Int(timeArrivingBus)) minutos y el tiempo estimado de desplazamiento hasta la parada más cercana es de \(Int(travelTime)) minutos. Puede que no alcances a llegar a tiempo.", preferredStyle: .alert)
                let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: { action in
                    self.substractUserPoints()
                } )
                alert.addAction(aceptar)
                let cancelar = UIAlertAction(title: "Cambiar bus", style: .cancel, handler: { action in
                    self.changeBusSelected()
                })
                alert.addAction(cancelar)
                present(alert, animated: true, completion: nil)
            }
            showingSelectedBus = true
            updateTablesSize(0)
            selectedBusTableView.reloadData()
        } else if tableView == favoritesTableView {
            searchBarBus.text = favorites[indexPath.row].busRouteName
            selectRoute(for: self.searchBarBus.text!)
            startedSearching = false
            updateTablesSize(0)
            substractUserPoints()
        }
    }
    
    func changeBusSelected(){
        showingSelectedBus = false
        showingTimes = true
        updateTablesSize(0)
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
    }
    
    func doFecthOfTimes(for stop: String){
        cancelButtonPressed = false
        //Do the firestore request of the bus times.
        let db = Firestore.firestore()
        let route = searchBarBus.text!
        db.collection("bus_stops").document(stop).collection(route).addSnapshotListener { (querySnapshot, err) in
            if self.busRoutesTimes.count > 0 {
                for (index, document) in querySnapshot!.documents.enumerated(){
                    let currentValue = self.busRoutesTimes[index]
                    let newValue = (document.get("arrivesIn") as? Int)!
                    if currentValue != newValue {
                        self.busRoutesTimes[index] = newValue
                        if index == self.numberOfBusSelected {
                            self.notifyUser = true
                        }
                    }
                }
            } else {
                for document in querySnapshot!.documents{
                    self.busRoutesTimes.append((document.get("arrivesIn") as? Int)!)
                }
                
                if !self.cancelButtonPressed{
                    self.busTimesTableView.reloadData()
                    self.showingTimes = true
                    self.loadingIndicator.stopAnimating()
                    self.updateTablesSize(0)
                    if self.favorites.count > 0 {
                        var isInFavorites = false
                        for favorite in self.favorites {
                            if favorite.busRouteName! == route {
                                //Shows the favorites button
                                isInFavorites = true
                                break
                            }
                        }
                        if !isInFavorites {
                            self.favoritesButton.isHidden = false
                        }
                    } else {
                        self.favoritesButton.isHidden = false
                    }
                }
            }
            print(stop)
            if self.notifyUser {
                //Notification settings
                let content = UNMutableNotificationContent()
                content.title = "¡El tiempo del bus cambió!"
                content.body = "El bus que seleccionaste cambio su tiempo estimado de llegada"
                content.sound = UNNotificationSound.default
                
                let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
                
                let request = UNNotificationRequest(identifier: "busTimeChanged", content: content, trigger: trigger)
                
                UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    //updates the height of the table if needed (Less than 4 elements). Also hides it if necessary.
    func updateTablesSize(_ numberOfRows: Int){
        
        if startedSearching {
            self.currentUserPoints.isHidden = true
            var height = CGFloat(100)
            if numberOfRows < 4 {
                let numberOfRowsCGFloat = CGFloat(numberOfRows)
                height = numberOfRowsCGFloat * 50
            }
            
            UIView.animate(withDuration: 0.5) {
                self.busRoutesTableHeight.constant = height
                self.busRoutesTableView.setNeedsUpdateConstraints()
            }
        } else {
            self.busRoutesTableHeight.constant = 0
            self.busRoutesTableView.setNeedsUpdateConstraints()
            self.currentUserPoints.isHidden = false
        }
        
        //Updates the times table heigth to show it when searching is done
        if showingTimes {
            self.busTimesTableHeight.constant = 250
            self.busTimesTableView.setNeedsUpdateConstraints()
        } else {
            self.busTimesTableHeight.constant = 0
            self.busTimesTableView.setNeedsUpdateConstraints()
        }
        
        if showingFavorites && favorites.count > 0{
            let numberOfRowsCGFloat = CGFloat(favorites.count)
            self.favoritesTableHeight.constant = numberOfRowsCGFloat * 50 + 50
            self.favoritesTableView.setNeedsUpdateConstraints()
        } else {
            self.favoritesTableHeight.constant = 0
            self.favoritesTableView.setNeedsUpdateConstraints()
        }
        
        if showingSelectedBus {
            self.selectedBusTableHeight.constant = 100
            self.selectedBusTableView.setNeedsUpdateConstraints()
        } else {
            self.selectedBusTableHeight.constant = 0
            self.selectedBusTableView.setNeedsUpdateConstraints()
        }
    }
}

//MARK: UISearchBarDelegate
//Extension to Search Bar related methods
extension ViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            startedSearching = false
            updateTablesSize(0)
        } else {
            searchedBus = busRoutesArr.filter({$0.lowercased().localizedStandardContains(searchText.lowercased())})
            searching = true
            startedSearching = true
            busRoutesTableView.reloadData()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        startedSearching = false
        searching = false
        showingTimes = false
        favoritesButton.isHidden = true
        showingFavorites = true
        searchBar.text = ""
        busRoutesTableView.reloadData()
        DismissKeyboard()
        notifyUser = false
        busRoutesTimes.removeAll()
        cancelButtonPressed = true
        numberOfBusSelected = 5
        showingSelectedBus = false
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        favoritesButton.isHidden = true
        startedSearching = true
        showingSelectedBus = false
        showingTimes = false
        showingFavorites = false
        busRoutesTableView.reloadData()
    }
}

// MARK: - CLLocationManagerDelegate
extension ViewController: CLLocationManagerDelegate {
    func requestLocation() -> CLLocation{
        let status = CLLocationManager.authorizationStatus()
        guard status == .authorizedWhenInUse || status == .authorizedAlways else {
            locationManager.requestWhenInUseAuthorization()
            locationManager.requestAlwaysAuthorization()
            return requestLocation()
        }
        return locationManager.location!
    }
    
    // 2
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        // 3
        guard status == .authorizedWhenInUse || status == .authorizedAlways else {
            locationManager.requestWhenInUseAuthorization()
            locationManager.requestAlwaysAuthorization()
            return
        }
        locationManager.startUpdatingLocation()
        mapView.showsUserLocation = true
    }
    
    // 6
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        
        mapView.setCenter(location.coordinate,zoomLevel: 15, animated: true)
        
        print("the updated location is \(location.coordinate)")
        
        locationManager.stopUpdatingLocation()
    }
}

// MARK: - MGLMapViewDelegate
extension ViewController: MGLMapViewDelegate {
    
    func mapViewDidFinishLoadingMap(_ mapView: MGLMapView) {
        // Start downloading tiles and resources for z13-16.
        startOfflinePackDownload()
    }
    
    
    func mapView(_ mapView: MGLMapView, imageFor annotation: MGLAnnotation) -> MGLAnnotationImage? {
        // Try to reuse the existing ‘parada’ annotation image, if it exists.
        var annotationImage = mapView.dequeueReusableAnnotationImage(withIdentifier: "parada")
        
        if annotationImage == nil {
            var image = UIImage(named: "parada")!
            
            // The anchor point of an annotation is currently always the center. To
            // shift the anchor point to the bottom of the annotation, the image
            // asset includes transparent bottom padding equal to the original image
            // height.
            //
            // To make this padding non-interactive, we create another image object
            // with a custom alignment rect that excludes the padding.
            image = image.withAlignmentRectInsets(UIEdgeInsets(top: 0, left: 0, bottom: image.size.height, right: 0))
            
            // Initialize the ‘parada’ annotation image with the UIImage we just loaded.
            annotationImage = MGLAnnotationImage(image: image, reuseIdentifier: "parada")
        }
        
        return annotationImage
    }
    
    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
        return true
    }
    
    // Calculate route to be used for navigation
    func calculateRoute(from origin: CLLocationCoordinate2D,
                        to destination: CLLocationCoordinate2D,
                        completion: @escaping (Route?, Error?) -> ()) {
        
        // Coordinate accuracy is the maximum distance away from the waypoint that the route may still be considered viable, measured in meters. Negative values indicate that a indefinite number of meters away from the route and still be considered viable.
        let origin = Waypoint(coordinate: origin, coordinateAccuracy: -1, name: "Start")
        let destination = Waypoint(coordinate: destination, coordinateAccuracy: -1, name: "Finish")
        
        // Specify that the route is intended for automobiles avoiding traffic
        let options = NavigationRouteOptions(waypoints: [origin, destination], profileIdentifier: .walking)
        
        // Generate the route object and draw it on the map
        _ = Directions.shared.calculate(options) { [unowned self] (waypoints, routes, error) in
            self.directionsRoute = routes?.first
            
            if self.directionsRoute != nil {
                // Draw the route on the map after creating it
               self.drawRoute(route: self.directionsRoute!)
               
               let travelTimeFormatter = DateComponentsFormatter()
               travelTimeFormatter.unitsStyle = .short
               let formattedTravelTime = travelTimeFormatter.string(from: self.directionsRoute!.expectedTravelTime)
               var formattedTravelTime2 = formattedTravelTime
               self.timeOfTravel = (formattedTravelTime2?.remove(at: (formattedTravelTime?.startIndex.self)!).lowercased())!
               print("ETA: \(formattedTravelTime!)")
            } else {
                _ = self.showAlert()
                self.searchBarCancelButtonClicked(self.searchBarBus)
            }
        }
    }
    
    func drawRoute(route: Route) {
        guard route.coordinateCount > 0 else { return }
        // Convert the route’s coordinates into a polyline
        var routeCoordinates = route.coordinates!
        let polyline = MGLPolylineFeature(coordinates: &routeCoordinates, count: route.coordinateCount)
        // If there's already a route line on the map, reset its shape to the new route
        if let source = mapView.style?.source(withIdentifier: "route-source") as? MGLShapeSource {
            source.shape = polyline
        } else {
            let source = MGLShapeSource(identifier: "route-source", features: [polyline], options: nil)
            
            // Customize the route line color and width
            let lineStyle = MGLLineStyleLayer(identifier: "route-style", source: source)
            lineStyle.lineColor = NSExpression(forConstantValue: #colorLiteral(red: 0.9607843137, green: 0.3843137255, blue: 0.02745098039, alpha: 1))
            lineStyle.lineWidth = NSExpression(forConstantValue: 3)
            
            // Add the source and style layer of the route line to the map
            mapView.style?.addSource(source)
            mapView.style?.addLayer(lineStyle)
        }
        
    }
    
    
    
}

//MARK:Extension methods

extension ViewController {
    
    @objc func offlinePackProgressDidChange(notification: NSNotification) {
        // Get the offline pack this notification is regarding,
        // and the associated user info for the pack; in this case, `name = My Offline Pack`
        if let pack = notification.object as? MGLOfflinePack,
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: pack.context) as? [String: String] {
            let progress = pack.progress
            // or notification.userInfo![MGLOfflinePackProgressUserInfoKey]!.MGLOfflinePackProgressValue
            let completedResources = progress.countOfResourcesCompleted
            let expectedResources = progress.countOfResourcesExpected
            
            // Calculate current progress percentage.
            let progressPercentage = Float(completedResources) / Float(expectedResources)
            
            // Setup the progress bar.
            if offlineMapProgress == nil {
                offlineMapProgress = UIProgressView(progressViewStyle: .default)
                let frame = view.bounds.size
                offlineMapProgress.frame = CGRect(x: frame.width / 4, y: frame.height * 0.75, width: frame.width / 2, height: 10)
                view.addSubview(offlineMapProgress)
            }
            print(progressPercentage)
            offlineMapProgress.progress = progressPercentage
            
            if progressPercentage == 1 {
                offlineMapProgress.removeFromSuperview()
                offlineMapProgress = nil
            }
            
            // If this pack has finished, print its size and resource count.
            if completedResources == expectedResources {
                let byteCount = ByteCountFormatter.string(fromByteCount: Int64(pack.progress.countOfBytesCompleted), countStyle: ByteCountFormatter.CountStyle.memory)
                print("Offline pack “\(userInfo["name"] ?? "unknown")” completed: \(byteCount), \(completedResources) resources")
            } else {
                // Otherwise, print download/verification progress.
                print("Offline pack “\(userInfo["name"] ?? "unknown")” has \(completedResources) of \(expectedResources) resources — \(progressPercentage * 100)%.")
            }
        }
    }
    
    @objc func offlinePackDidReceiveError(notification: NSNotification) {
        if let pack = notification.object as? MGLOfflinePack,
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: pack.context) as? [String: String],
            let error = notification.userInfo?[MGLOfflinePackUserInfoKey.error] as? NSError {
            print("Offline pack “\(userInfo["name"] ?? "unknown")” received error: \(error.localizedFailureReason ?? "unknown error")")
        }
    }
    
    @objc func offlinePackDidReceiveMaximumAllowedMapboxTiles(notification: NSNotification) {
        if let pack = notification.object as? MGLOfflinePack,
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: pack.context) as? [String: String],
            let maximumCount = (notification.userInfo?[MGLOfflinePackUserInfoKey.maximumCount] as AnyObject).uint64Value {
            print("Offline pack “\(userInfo["name"] ?? "unknown")” reached limit of \(maximumCount) tiles.")
        }
    }
    
    
    func startOfflinePackDownload() {
        
        
        let region = MGLTilePyramidOfflineRegion(styleURL: URL(string: "mapbox://styles/pedrito8472/ck2nmfofh1gbc1clohqryhtdx"), bounds: MGLCoordinateBounds(sw: CLLocationCoordinate2D(latitude: 4.4811, longitude: -74.26368), ne: CLLocationCoordinate2D(latitude: 4.77779, longitude: -73.9797)), fromZoomLevel: 2, toZoomLevel: 18)
        
        let userInfo = ["name": "Offline Bogota map"]
        do{
            let context = try NSKeyedArchiver.archivedData(withRootObject: userInfo, requiringSecureCoding: true)
            MGLOfflineStorage.shared.addPack(for: region, withContext: context) { (pack, error) in
                guard error == nil else {
                    print("EFE")
                    return
                }
                pack!.resume()
            }
        } catch {
            print("problems creating the context")
        }
    }
    
    func HideKeyboard() {
        let Tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(DismissKeyboard))
        view.addGestureRecognizer(Tap)
        
        //reset the table view to hide it
        startedSearching = false
    }
    
    @objc func DismissKeyboard(){
        view.endEditing(true)
    }
    
    func selectRoute(for route: String) {
        
        //Loader indicator
        loadingIndicator.center = self.view.center
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.large
        loadingIndicator.color = .black
        view.addSubview(loadingIndicator)
        loadingIndicator.startAnimating()
        
        let db = Firestore.firestore()
        db.collection("bus_stops").whereField("route", isEqualTo: route).getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("EFE, \(err)")
            } else {
                if let annotations = self.mapView.annotations {
                    self.mapView.removeAnnotations(annotations)
                }
                self.selectedStops.removeAll()
                self.selectedStops.append(contentsOf: querySnapshot!.documents)
                
                let closestStop = self.getClosestStop()
                self.showStop(for: closestStop)
                self.doFecthOfTimes(for: closestStop.documentID)
                let geoPoint = closestStop.get("location") as! GeoPoint
                let stopCoord = CLLocationCoordinate2D(latitude: geoPoint.latitude, longitude: geoPoint.longitude)
                self.calculateRoute(from: self.requestLocation().coordinate, to: stopCoord) { (route, error) in
                    if error != nil {
                        
                    }
                }
            }
        }
        
    }
    
    func showStop(for stop: DocumentSnapshot) {
        let stopAn = MGLPointAnnotation()
        let geoPoint = stop.get("location") as! GeoPoint
        stopAn.coordinate = CLLocationCoordinate2D(latitude: geoPoint.latitude, longitude: geoPoint.longitude)
        stopAn.title = stop.documentID
        self.mapView.addAnnotation(stopAn)
        
    }
    
    
    
    func getClosestStop() -> DocumentSnapshot {
        let userLocation = requestLocation()
        let userLatitude = userLocation.coordinate.latitude
        let userLongitude = userLocation.coordinate.longitude
        
        var closestStop = selectedStops[0]
        var minDist: Double = 99999999999
        for stop in selectedStops{
            
            let coords = stop.get("location") as! GeoPoint
            let delta = abs(userLongitude-coords.longitude) + abs(userLatitude-coords.latitude)
            if (delta < minDist) {
                minDist = delta
                closestStop = stop
            }
        }
        
        return closestStop
    }
    
    
}

extension UISearchBar {

    func getTextField() -> UITextField? { return value(forKey: "searchField") as? UITextField }
    func set(textColor: UIColor) { if let textField = getTextField() { textField.textColor = textColor } }
    func setPlaceholder(textColor: UIColor) { getTextField()?.setPlaceholder(textColor: textColor) }
}

private extension UITextField {

    private class Label: UILabel {
        private var _textColor = UIColor.lightGray
        override var textColor: UIColor! {
            set { super.textColor = _textColor }
            get { return _textColor }
        }

        init(label: UILabel, textColor: UIColor = .lightGray) {
            _textColor = textColor
            super.init(frame: label.frame)
            self.text = label.text
            self.font = label.font
        }

        required init?(coder: NSCoder) { super.init(coder: coder) }
    }

    var placeholderLabel: UILabel? { return value(forKey: "placeholderLabel") as? UILabel }

    func setPlaceholder(textColor: UIColor) {
        guard let placeholderLabel = placeholderLabel else { return }
        let label = Label(label: placeholderLabel, textColor: textColor)
        setValue(label, forKey: "placeholderLabel")
    }

    func getClearButton() -> UIButton? { return value(forKey: "clearButton") as? UIButton }
}
